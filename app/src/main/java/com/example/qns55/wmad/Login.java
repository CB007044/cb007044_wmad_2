package com.example.qns55.wmad;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Login extends AppCompatActivity {

    SQLiteDatabase db;
    SQLiteOpenHelper OpenHelper;
    Button _loginBTN;
    EditText _loginPW, _loginEM;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button _logincoa;

        {
            _logincoa = (Button) findViewById(R.id.logincoa);
        }

        _logincoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, MainMenu.class));
            }
        });
        _loginEM=(EditText)findViewById(R.id.loginEM);
        _loginPW=(EditText)findViewById(R.id.loginPW);
        _loginBTN=(Button)findViewById(R.id.loginBTN);
        _loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=_loginEM.getText().toString();
                String password=_loginPW.getText().toString();

                cursor=db.rawQuery("Select * From "+Database.TABLE_NAME+" Where"+Database.COL_3+"=? AND"+Database.COL_5, new String[]{email, password});
                if(cursor!=null){
                    if(cursor.getCount()>0){
                        cursor.moveToNext();

                        {
                            _loginBTN = (Button) findViewById(R.id.loginBTN);
                        }

                        _loginBTN.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(Login.this, MainMenu.class));
                            }
                        });
                    }
                    else if(email=="admin"){
                        if(password=="pass"){
                        cursor.moveToNext();

                        {
                            _loginBTN = (Button) findViewById(R.id.loginBTN);
                        }

                        _loginBTN.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(Login.this, MainMenu.class));
                            }
                        });

                    }}

                    else if(email==password){
                        Toast.makeText(getApplicationContext(),"Invalid login details",Toast.LENGTH_LONG).show();
                    }

                }



            }


        });


    }
}

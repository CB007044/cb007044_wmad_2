package com.example.qns55.wmad;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    SQLiteOpenHelper OpenHelper;
    SQLiteDatabase db;
    Button _regBTN;
    EditText _regFN, _regLN, _regPW, _regEM, _regPN;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_style_omega);
        OpenHelper=new Database(this);
        _regBTN=(Button)findViewById(R.id.regBTN);
       _regFN=(EditText)findViewById(R.id.regFN);
        _regLN=(EditText)findViewById(R.id.regLN);
        _regPW=(EditText)findViewById(R.id.regPW);
        _regEM=(EditText)findViewById(R.id.regEM);
        _regPN=(EditText)findViewById(R.id.regPN);
        _regBTN.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View v){
                    db=OpenHelper.getWritableDatabase();
                    String regFN=_regFN.getText().toString();
                    String regLN=_regLN.getText().toString();
                    String regPW=_regPW.getText().toString();
                    String regEM=_regEM.getText().toString();
                    String regPN=_regPN.getText().toString();
                    insertdata(regFN, regLN, regPW, regEM, regPN);
                Toast.makeText(getApplicationContext(), "Registration Complete", Toast.LENGTH_LONG).show();


            }

        });


    }
    public void insertdata(String regFN, String regLN, String regPW, String regEM, String regPN){
        ContentValues contentValues=new ContentValues();
        contentValues.put(Database.COL_1, regFN);
        contentValues.put(Database.COL_2, regLN);
        contentValues.put(Database.COL_3, regEM);
        contentValues.put(Database.COL_4, regPN);
        contentValues.put(Database.COL_5, regPW);
        long id= db.insert(Database.TABLE_NAME, null, contentValues);

    }
}

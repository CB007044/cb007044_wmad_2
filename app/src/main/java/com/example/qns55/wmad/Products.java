package com.example.qns55.wmad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

public class Products extends Activity implements AdapterView.OnItemClickListener {


    int[] images= {R.drawable.shirtb, R.drawable.trouserb, R.drawable.dressb, R.drawable.jeansb, R.drawable.shoesmb, R.drawable.shoesfb, R.drawable.backpackb};
    String[] texts={"Shirt", "Trouser", "Dress", "Jeans","Shoes(Men)", "Shoes(Women)", "Backpack"};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_products);

        ListView listview = (ListView) findViewById(R.id.listView1);
        listview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        Intent intent = new Intent();
        intent.setClass(this, ItemDetail.class);
        intent.putExtra("position", position);
        intent.putExtra("id", id);
        startActivity(intent);
    }
}

package com.example.qns55.wmad;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class OpeningPage extends Activity {
    SQLiteOpenHelper OpenHelper;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening_page);


        Button _oplogin;

        {
            _oplogin = (Button) findViewById(R.id.oplogin);
        }

        _oplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OpeningPage.this, Login.class));
            }
        });

        Button _opregister;

        {
            _opregister = (Button) findViewById(R.id.opregister);
        }
        _opregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OpeningPage.this, Register.class));
            }
        });



    }









}


